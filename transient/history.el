((magit-am
  ("--3way"))
 (magit-branch nil)
 (magit-commit nil)
 (magit-diff
  ("--no-ext-diff" "--stat"))
 (magit-dispatch nil)
 (magit-fetch nil)
 (magit-log
  ("-n256" "--graph" "--decorate")
  ("-n256"
   ("--" "shopify/graphql/graphql.ts")
   "--graph" "--decorate"))
 (magit-merge nil)
 (magit-pull nil)
 (magit-push nil)
 (magit-rebase nil)
 (magit-remote
  ("-f"))
 (magit-remote\.<remote>\.*url "https://gitlap.com/sierra-madre-supplies/old-envy-hcf-theme.git" "https://gitlab.com/sierra-madre-supplies/hcf-theme.git" "git@gitlab.com:sierra-madre-supplies/email-signatures.git" "https://gitlab.com/sierra-madre-supplies/the-old-database.git" "git@github.com:BroClements/CS364_2020Win.git" "https://github.com/caleb5baird/CS364_2020Win.git" "https://github.com/BroClements/CS364_2020Win.git" "git@gitlab.com:caleb5baird/emacs-config.git" "https://github.com/syl20bnr/spacemacs" "git@gitlab.com:caleb5baird/spacemacs-config.git")
 (magit-reset nil)
 (magit-revert
  ("--edit"))
 (magit-stash nil)
 (magit:-- "shopify/graphql/graphql.ts," ""))
